import Service from './../models/service.int';
import axios from 'axios';

export default class ServiceController extends Service {
  constructor(id, type, destination, interval) {
    super(id, type, destination, interval);
    this._state = 0;
    this._error = null;
    this._errorText = null;
    this._timeout = null;
  }

  async mk_https_req() {
    try {
      if (this._state === 1) {
        this._timeout = setTimeout(async () => {
          const resp = await axios.get(this.destination);
          if (resp && resp.status === 200) {
            console.log(`${this.destination} is alive!`);
          }
          this.mk_https_req();
        }, this.interval * 1000);
      } else {
        return false;
      }
    } catch(e) {
      this._error = 1;
      this._errorText = e.toString();
      return false;
    }
  }

  start() {
    this._state = 1;
    if (this.type === 'https') {
      this.mk_https_req();
    }
  }

  pause() {
    this._state = 0;
    clearTimeout(this._timeout);
    this._timeout = null;
  }

}
