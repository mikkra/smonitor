FROM node:12-alpine
WORKDIR /w
COPY package.json package.json
RUN npm i -q
COPY . .
CMD ["npm", "start"]
