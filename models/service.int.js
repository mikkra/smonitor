export default class Service {
  constructor(id = 1, type = 'https', destination = 'https://google.com/', interval = 60) {
    this.id = Number(id);
    this.type = type.toLowerCase();
    this.destination = destination;
    this.interval = Number(interval);
    this._state = 0;
  }
}
